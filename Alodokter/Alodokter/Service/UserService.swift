//
//  UserService.swift
//  Alodokter
//
//  Created by arizal on 04/09/18.
//  Copyright © 2018 Arizal rizal. All rights reserved.
//

import ObjectMapper
import RealmSwift

struct UserService {
    
    func registerUser(_ mail: String, _ password: String){
        let tokenObj = NSMutableDictionary()
        let tokenStr = self.getToken()
        
        tokenObj.setValue(tokenStr, forKey: "token")
        tokenObj.setValue(mail, forKey: "mail")
        tokenObj.setValue(password, forKey: "password")
        
        let userTokenModel = Mapper<UserTokenModel>().map(JSONObject: tokenObj)
        
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(userTokenModel!, update: true)
        }
        
    }
    
    func getToken() -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var token = ""
        
        for _ in 0 ..< 15 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            token += NSString(characters: &nextChar, length: 1) as String
        }
        
        return token
    }
}
