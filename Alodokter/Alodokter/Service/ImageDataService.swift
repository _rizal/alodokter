//
//  ImageDataService.swift
//  Alodokter
//
//  Created by arizal on 04/09/18.
//  Copyright © 2018 Arizal rizal. All rights reserved.
//

import ObjectMapper
import RealmSwift

struct ImageDataService {
    
    func getDetailImageData(cityName : String) -> [String]{
        
        let dataImage = NSMutableDictionary()
        dataImage.setValue(["seaworld-ic", "tamanmini-ic", "ancol-ic"], forKey: "jakarta-ic")
        dataImage.setValue(["cikole-ic", "ciwalk-ic", "tangkubanperahu-ic"], forKey: "bandung-ic")
        dataImage.setValue(["malioboro-ic", "tugu-ic", "borobudur-ic"], forKey: "jogja-ic")
        
        return dataImage.value(forKey: cityName) as! [String]
    }
    
    func getCityImageData() -> [String]{
        return ["jakarta-ic", "bandung-ic", "jogja-ic"]
    }
    
}
