//
//  LoginController.swift
//  Alodokter
//
//  Created by arizal on 03/09/18.
//  Copyright © 2018 Arizal rizal. All rights reserved.
//

import UIKit
import SwiftSpinner

class LoginViewController : UIViewController, UITextFieldDelegate{
    
    //MARK: - Vars
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var buttonMasuk: UIButton!
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        self.configureUI()
        self.bindElement()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: - Private
    func configureUI(){
        buttonMasuk.layer.borderWidth = 1
        buttonMasuk.layer.borderColor = UIColor.black.cgColor
        buttonMasuk.layer.cornerRadius = 5
        emailTextfield.layer.borderWidth = 1
        emailTextfield.layer.borderColor = UIColor.black.cgColor
        emailTextfield.layer.cornerRadius = 5
        passwordTextfield.layer.borderWidth = 1
        passwordTextfield.layer.borderColor = UIColor.black.cgColor
        passwordTextfield.layer.cornerRadius = 5
        
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    func bindElement(){
        buttonMasuk.addTarget(self, action: #selector(onClickedMasuk), for: .touchUpInside)
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
    }
    
    @objc func onClickedMasuk(){
        SwiftSpinner.show("loading...")
        if emailTextfield.text == "" || passwordTextfield.text == ""{
            self.showAlert(message: "Email or password cannot empty.")
        }
        else if !self.isValidEmail(testStr: emailTextfield.text!){
            self.showAlert(message: "Invalid email format.")
        }
        else{
            UserService().registerUser(emailTextfield.text!, passwordTextfield.text!)
            let homeScreenController = HomeScreenController()
            self.navigationController?.pushViewController(homeScreenController, animated: true)
        }
        SwiftSpinner.hide()
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func showAlert(message : String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in}
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        onClickedMasuk()
        return true
    }
}
