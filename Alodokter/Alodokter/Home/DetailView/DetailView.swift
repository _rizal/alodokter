//
//  DetailView.swift
//  Alodokter
//
//  Created by arizal on 04/09/18.
//  Copyright © 2018 Arizal rizal. All rights reserved.
//

import UIKit

class DetailView : UIView, UIScrollViewDelegate{
    //MARK: - Vars
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    
    var dataImage = ["boy-ic", "boy-ic", "boy-ic"]
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadView()
        self.configScrollView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Private
    func loadView(){
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "DetailView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        self.addSubview(view!)
        view?.frame = self.bounds
        view?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func configScrollView(){
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.contentSize = CGSize(width: containerView.bounds.width * CGFloat(dataImage.count), height: 350)
        scrollView.showsHorizontalScrollIndicator = false
        
        for (index, item) in dataImage.enumerated(){
            let contentPosition = index * Int(containerView.frame.width)
            
            let imageView = UIImageView(frame: CGRect(x: CGFloat(contentPosition), y: 0, width: containerView.bounds.width, height: 350))
            let image = UIImage(named: item)
            imageView.image = image
            scrollView.addSubview(imageView)
        }
    }
    
    //MARK: - Public
    open func setContentData(data : [String]){
        dataImage = data
        configScrollView()
    }
    
    //MARK: - ScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.currentPage = Int(page)
    }
}
