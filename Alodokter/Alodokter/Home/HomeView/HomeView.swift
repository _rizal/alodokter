//
//  HomeViewController.swift
//  Alodokter
//
//  Created by arizal on 04/09/18.
//  Copyright © 2018 Arizal rizal. All rights reserved.
//

import UIKit

protocol HomeViewDelegate {
    func onSelectedTable(cityName : String)
}

class HomeView : UIView{
    
    //MARK: - Vars
    @IBOutlet weak var tableView: UITableView!
    
    var dataTable : [String]!
    var homeViewDelegate : HomeViewDelegate!
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadView()
        self.bindElement()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Private
    func loadView(){
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "HomeView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        self.addSubview(view!)
        view?.frame = self.bounds
        view?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func bindElement(){
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
    }
    
    //MARK: - Public
    open func setDelegate(homeViewDelegate : HomeViewDelegate){
        self.homeViewDelegate = homeViewDelegate
    }
    
    open func setDataTable(data : [String]){
        dataTable = data
        tableView.reloadData()
    }
}

//MARK: - Extension
extension HomeView : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let homeCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        
        let imageName = dataTable[indexPath.row]
        homeCell.imageContent.image = UIImage(named: imageName)

        return homeCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard homeViewDelegate != nil else { return }
        homeViewDelegate.onSelectedTable(cityName: dataTable[indexPath.row])
    }
}

extension HomeView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.width
    }
    
}
