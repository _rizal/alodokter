//
//  HomeTableViewCell.swift
//  Alodokter
//
//  Created by arizal on 04/09/18.
//  Copyright © 2018 Arizal rizal. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    //MARK: - Vars
    @IBOutlet weak var imageContent: UIImageView!
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        imageContent.layer.borderColor = UIColor.black.cgColor
        imageContent.layer.borderWidth = 1
    }
}
