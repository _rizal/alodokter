//
//  HomeScreenController.swift
//  Alodokter
//
//  Created by arizal on 04/09/18.
//  Copyright © 2018 Arizal rizal. All rights reserved.
//
import UIKit
import SwiftSpinner

class HomeScreenController : UIViewController, HomeViewDelegate{
    
    //MARK: - Vars
    @IBOutlet weak var contentContainer: UIView!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    
    var homeView : HomeView!
    var profileView : ProfileView!
    var detailView : DetailView!
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.bindElement()
        self.onClickedHome()
        
        navigationItem.setHidesBackButton(true, animated: true)
    }
    
    //MARK: - Private
    func configureUI(){
        self.title = "My Apps"
        homeButton.layer.borderWidth = 1
        homeButton.layer.borderColor = UIColor.black.cgColor
        profileButton.layer.borderWidth = 1
        profileButton.layer.borderColor = UIColor.black.cgColor
    }
    
    func bindElement(){
        homeButton.addTarget(self, action: #selector(onClickedHome), for: .touchUpInside)
        profileButton.addTarget(self, action: #selector(onClickedProfile), for: .touchUpInside)
        
    }
    
    @objc func onClickedHome(){
        SwiftSpinner.show("loading...")
        if homeView == nil{
            homeView = HomeView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: contentContainer.frame.size.height))
            homeView.setDelegate(homeViewDelegate: self)
        }
        
        let dataTable = ImageDataService().getCityImageData()
        homeView.setDataTable(data: dataTable)
        homeView.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
        changeView(view: homeView)
        SwiftSpinner.hide()
    }
    
    @objc func onClickedProfile(){
        SwiftSpinner.show("loading...")
        if profileView == nil{
            profileView = ProfileView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: contentContainer.frame.size.height))
        }
        
        changeView(view: profileView)
        SwiftSpinner.hide()
    }
    
    func changeView(view : UIView){
        for view in contentContainer.subviews{
            view.removeFromSuperview()
        }
        
        contentContainer.addSubview(view)
    }
    
    //MARK: - HomeViewDelegate
    func onSelectedTable(cityName: String) {
        SwiftSpinner.show("loading...")
        let data = ImageDataService().getDetailImageData(cityName: cityName)
        if detailView == nil{
            detailView = DetailView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: contentContainer.frame.size.height))
        }
        
        detailView.scrollView.contentOffset = CGPoint(x: 0.0, y: 0.0)
        detailView.setContentData(data: data)
        changeView(view: detailView)
        SwiftSpinner.hide()
    }
    
}
