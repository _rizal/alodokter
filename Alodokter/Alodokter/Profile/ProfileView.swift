//
//  ProfileView.swift
//  Alodokter
//
//  Created by arizal on 04/09/18.
//  Copyright © 2018 Arizal rizal. All rights reserved.
//

import UIKit

class ProfileView : UIView{
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Private
    func loadView(){
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ProfileView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        
        self.addSubview(view!)
        view?.frame = self.bounds
        view?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
