//
//  UserTokenModel.swift
//  Alodokter
//
//  Created by arizal on 04/09/18.
//  Copyright © 2018 Arizal rizal. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class UserTokenModel : Object, Mappable{
    //MARK: - Vars
    @objc dynamic var token : String = ""
    @objc dynamic var mail : String = ""
    @objc dynamic var password : String = ""
    @objc dynamic var id = 0
    
    //MARK: - Impl. of Mappable protocol
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override open static func primaryKey() -> String? {
        return "id"
    }
    
    public func mapping(map: Map) {
        token <- map["token"]
        mail <- map["mail"]
        password <- map["password"]
    }
}
